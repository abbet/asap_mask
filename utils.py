from skimage.transform import resize
from tqdm import tqdm
import sys
import numpy as np
from scipy.ndimage import laplace
from scipy.ndimage.morphology import binary_dilation
from scipy.ndimage import generate_binary_structure


def load_mir(path):
    """
    Load package/module for multiresolution image interface from ASAP folder.
    :param path: (str) Path to ASAP python interface wrapper.
    """
    try:
        # Try to include MIR package from ASAP
        sys.path.insert(0, path)
        mir = __import__("multiresolutionimageinterface", fromlist=[''])
        return mir

    except ModuleNotFoundError:
        print("No MIR (Multi-resolution Image Interface) package found")
        return None


def convert_to_edges(raw_mask):
    """
    Convert input image to edge map using a laplace filter. The contour width is further increased with binary
    dilation for visualization purposes.
    :param raw_mask: (MxN ndarray) Generated array to convert as mask. Mask values should be positive integer values.
    :return:
        - edge_mask: (MxN ndarray) Computed edge map of the input mask
    """
    struct = generate_binary_structure(2, 2)
    edge_mask = (laplace(raw_mask) > 128).astype(raw_mask.dtype)
    edge_mask = binary_dilation(edge_mask, structure=struct).astype(edge_mask.dtype)
    return edge_mask


def generate_toy_mask_from_image(mir, path):
    """

    :param mir: (module) Instance of multiresolution image interface.
    :param path: (str) path to image
    :return:
        - raw_mask: Dummy mask generated to match image size
        - out_dims: Original output dimensions from source image
    """

    level = 3
    reader = mir.MultiResolutionImageReader()
    image = reader.open(path)
    level_dims = image.getLevelDimensions(level)
    tile = image.getUCharPatch(0, 0, level_dims[0], level_dims[1], level)
    tile_od = -np.log(np.clip(tile, 1, 254) / 255.)
    q = np.quantile(tile_od.mean(axis=2), [0.33, 0.66])
    raw_mask = np.zeros((tile_od.shape[0], tile_od.shape[1])).astype('ubyte')
    raw_mask[tile_od.mean(axis=2) < q[0]] = 1
    raw_mask[(tile_od.mean(axis=2) >= q[0]) & (tile_od.mean(axis=2) < q[1])] = 2
    raw_mask[tile_od.mean(axis=2) >= q[1]] = 3

    return raw_mask


def write_mask(mir, raw_mask, slide_shape, filename, tile_size=512):
    """
    Write a mask overlay for ASAP. Note that for better results, the ratio of the mask size and original slide
    should be a power of 2.
    :param mir: (module) Instance of multiresolution image interface.
    :param raw_mask: (MxN ndarray) Generated array to convert as mask. Mask values should be positive integer values.
    :param slide_shape: (tuple), width and height of original image slide.
    :param filename: (str) output path/filename of mask file.
    :param tile_size: (int) size of generated tiles. Default is 512.
    """

    # Write image output base information
    writer = mir.MultiResolutionImageWriter()
    writer.openFile(filename)
    writer.setTileSize(tile_size)
    writer.setCompression(mir.LZW)
    writer.setDataType(mir.UChar)
    writer.setInterpolation(mir.NearestNeighbor)
    writer.setColorType(mir.Monochrome)
    writer.writeImageInformation(slide_shape[0], slide_shape[1])

    # Level dims
    level_dims = (raw_mask.shape[1], raw_mask.shape[0])
    level_down = int(slide_shape[0]/level_dims[0])
    step_size = int(tile_size / int(level_down))

    for y in tqdm(range(0, level_dims[1], step_size), desc='Writing image patches'):
        for x in range(0, level_dims[0], step_size):
            write_tl = np.zeros((step_size, step_size), dtype='ubyte')
            cur_tl = raw_mask[y:y+step_size, x:x+step_size]
            write_tl[0:cur_tl.shape[0], 0:cur_tl.shape[1]] = cur_tl
            res_tl = resize(write_tl,
                            (tile_size, tile_size),
                            order=0, mode="constant", preserve_range=True).astype('ubyte')
            writer.writeBaseImagePart(res_tl.flatten())

    print('Finishing image ...')
    writer.finishImage()
    print('Image mask created and saved to {}'.format(filename))