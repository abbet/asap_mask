# Usage

Base function to create an ASAP image overlay. This work was adapted from:
* ASAP (https://computationalpathologygroup.github.io/ASAP/)
* ASAP Github (https://github.com/computationalpathologygroup/ASAP)
* ASAP Overlay documentation (https://academic.oup.com/gigascience/article/7/6/giy065/5026175)


First, you need to download ASAP from 
[release](https://github.com/computationalpathologygroup/ASAP/releases) folder. The install the package using the
 terminal and check output path:

```bash
% Install package
sudo dpkg -i ASAP-1.9-Linux-Ubuntu1804.deb
% Check installation path (should be /opt/ASAP) 
dpkg -L asap
```

You can now create a python environment with needed libraries: 
```bash
% Create env for ASAP
conda create --name wsi_mask python=3.6 numpy scikit-image tqdm
conda activate wsi_mask
```

To use the library in an external python code (without using sys package), you can add the library path to PYTHONPATH
```bash
PYTHONPATH="/opt/ASAP/bin":"${PYTHONPATH}"
export PYTHONPATH
```

### How to run

1. Dummy mask

To run the example download an example slide (e.g. test_128.tif) from [camelyon17](https://camelyon17.grand-challenge.org/) dataset and run:
```bash
python generate_mask.py --path_asap='/opt/ASAP/bin' --img_path test_128.tif --output test_128_mask.tif
```

2. Numpy mask

The numpy mask file should be a dictionary with the proper field. Here is an example of code to generate a dummy mask:
```python
import numpy as np
w, h = ..., ...
mask = np.ones(h, w)
# Save compressed version of the mask
np.savez_compressed("my_mask.npz", mask=mask)
```

Then you can use the command:
```bash
python generate_mask.py --path_asap='/opt/ASAP/bin' --img_path test_128.tif --output test_128_mask.tif --numpy_mask my_mask.npy
```

If you want 0 to be displayed as background in the viewer (transparent) use `add_offset=0` otherwise use `add_offset=1` to display all labels values including 0.