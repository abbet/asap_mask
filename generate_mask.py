import argparse
import numpy as np
from utils import write_mask, convert_to_edges, generate_toy_mask_from_image, load_mir


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--path_asap', dest='path_asap', type=str,
                    default='/opt/ASAP/bin',
                    help='Path to ASAP python interface wrapper')
parser.add_argument('--img_path', dest='img_path', type=str,
                    default='',
                    # default='/home/abbet/Desktop/BernCohortPart/001b_B2005.30530_C_HE.mrxs',
                    # default='/home/abbet/Desktop/BernCohortPart/003b_B2010.14359_I-D_HE.mrxs',
                    help='Path to *.tif file')
parser.add_argument('--numpy_mask', dest='numpy_mask', type=str,
                    default='',
                    # default='/home/abbet/Desktop/mask10c/003b_B2010.14359_I-D_HE.mrxs/mask_003b_B2010.14359_I-D_HE.mrxs_crf.npz',
                    # default='/media/abbet/DataSSD2/ieee_tmi/models/kather19-bern/sra_25_01/mask/001b_B2005.30530_C_HE.mrxs/mask_001b_B2005.30530_C_HE.mrxs_crf.npz',
                    help='Path to *.npy mask file to convert to ASAP format')
parser.add_argument('--output', dest='output', type=str,
                    default='/home/abbet/Downloads/mask_test.tif',
                    # default='/home/abbet/Downloads/mask_003b_B2010.14359_I-D_HE.tif',
                    help='Output mask *.tif file')

# Optional args
parser.add_argument('--tile_size', dest='tile_size',
                    type=int, default=512,
                    help='(Optional) Slide level to read, 0 is the original size.')
parser.add_argument('--label', dest='label', type=int, default=3,
                    help='(Optional) Choose whether to output only a specific label.')
parser.add_argument('--egdes', dest='egdes', type=int, default=0, choices=[0, 1],
                    help='(Optional) Generate only the outershape/edge of the mask.')
args = parser.parse_args()


def main():

    # Load MIR module
    mir = load_mir(path=args.path_asap)

    # Get original image size
    reader = mir.MultiResolutionImageReader()
    image_org = reader.open(args.img_path)
    org_dims = image_org.getLevelDimensions(0)

    if args.numpy_mask is None:
        # Generate dummy mask for example purpose
        print('No mask was provided, generate toy mask for example purpose')
        raw_mask = generate_toy_mask_from_image(mir, path=args.img_path)
    else:
        # Extract mask from numpy file (see readme to know input format)
        print('Loading mask from {} ...'.format(args.numpy_mask))
        raw_mask = np.load(args.numpy_mask)['mask']

    if args.label is not None:
        # Put all classes to 0 except label
        print('Selected mode label = {}'.format(args.label))
        raw_mask = (raw_mask == args.label).astype(raw_mask.dtype)

    if args.egdes:
        # Convert to edge map
        print('Computing mask edges')
        raw_mask = convert_to_edges(raw_mask)

    # Write multiresolution image mask
    write_mask(mir, raw_mask=raw_mask, slide_shape=org_dims, filename=args.output, tile_size=args.tile_size)


if __name__ == '__main__':
    main()
